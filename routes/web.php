<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Http\Request;
Route::get('/', function () {
    return view('index');
});

Route::get('/new/transaksi', function(){
	return view('transaksi.new');
});

Route::get('/transaksi', 'TransaksiController@index');
Route::get('/users', 'UserController@index');
Route::post('/create/transaksi', 'TransaksiController@create')->name('create-trans');

// Route untuk user yang baru register
Route::group(['prefix' => 'home', 'middleware' => ['auth']], function(){
	Route::get('/', function(){
		$data['role'] = \App\UserRole::whereUserId(Auth::id())->get();
		return view('home', $data);
	});
	Route::post('upgrade', function(Request $request){
		if($request->ajax()){
			$msg['success'] = 'false';
			$user = \App\User::find($request->id);
			if($user)
				$user->putRole($request->level);
				$msg['success'] = 'true';
			return response()
				->json($msg);
		}
	});
});
// Route untuk user yang superadmin
Route::group(['prefix' => 'superadmin', 'middleware' => ['auth','role:superadmin']], function(){
	Route::get('/', function(){
		$data['users'] = \App\User::whereDoesntHave('roles')->get();
		return view('admin', $data);
	});
});
// Route untuk user yang direksi
Route::group(['prefix' => 'direksi', 'middleware' => ['auth','role:direksi']], function(){
	Route::get('/', function(){
		return view('member');
	});
});
Auth::routes();


