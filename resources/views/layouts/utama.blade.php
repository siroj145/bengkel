<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="assets/logo/logo-as.png">

    <title>@yield ('title')</title>

    <!-- CSS -->
    <link href="../../assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- JS -->
    <script type="text/javascript" src="../../assets/js/jquery.min.js"></script>

  </head>
  <body>

    <header>
    <nav class="navbar navbar-default primary">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand">Bengkel</a>
        </div>
        <ul class="nav navbar-nav">
            <li class="nav-item">
              <a class="nav-link" href="/new/transaksi">Entry Data Transaksi</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/users">Manajemen Pengguna</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/transaksi">Transaksi Harian</a>
            </li>
        </ul>
      </div>  
    </nav>
  </header>
    
    @yield('body')
    @show
  
    <script type="text/javascript" src="../../assets/js/bootstrap.min.js"></script>
  </body>
</html>
