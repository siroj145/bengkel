@extends('layouts.utama')

@section('title', 'App Bengkel | Transaksi')

@section('body')
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="page-header">Transaksi</h1>
				<a href="/new/transaksi" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Transaksi Baru</a><br>
			</div>
				<table class="table table-striped">
					<thead>
						<th>Keterangan</th>
						<th>Debit</th>
						<th>Kredit</th>
						<th colspan="2">Aksi</th>
					</thead>
					<tbody>
						@foreach($transact as $tr)
						<td>{{$tr->keterangan}}</td>
						<td>{{$tr->kredit}}</td>
						<td>{{$tr->debit}}</td>
						<td><a href="/edit/transaksi/{{$tr->id}}" class="btn btn-warning"><span class="glyphicon glyphicon-refresh"></span> Edit</a></td>
						<td><a href="/hapus/transaksi/{{$tr->id}}" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> Hapus</a></td>
						@endforeach
					</tbody>
				</table>
		</div>
	</div>
@endsection