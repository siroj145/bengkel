@extends('layouts.utama')

@section('title', 'App Bengkel | Transaksi Baru')

@section('body')
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="page-header">Transaksi Baru</h1>
			</div>
			<form method="POST" action="{{ route('create-trans') }}">
				<div class="col-md-8">
					<div class="form-group">
						<label for="ket">Keterangan :</label>
						<textarea id="ket" name="keterangan" class="form-control"></textarea>
					</div>
					<div class="form-group">
						<label for="deb">Debit :</label>
						<input type="number" id="deb" name="debit" placeholder="Debit" class="form-control">
					</div>
					<div class="form-group">
						<label for="kre">Kredit :</label>
						<input type="number" id="kre" name="kredit" placeholder="Kredit" class="form-control">
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group">
						<button type="submit" name="sub" class="btn btn-primary">Tambah</button>
					</div>
				</div>
			</form>
		</div>
	</div>
@endsection