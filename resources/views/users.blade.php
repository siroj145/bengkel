@extends('layouts.utama')

@section('title', 'App Bengkel | Users')

@section('body')
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="page-header">Users</h1>
				<a href="/new/user" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> User Baru</a><br>
			</div>
				<table class="table table-striped">
					<thead>
						<th>Nama</th>
						<th>Email</th>
						<th colspan="2">Aksi</th>
					</thead>
					<tbody>
						@foreach($user as $us)
						<td>{{$us->name}}</td>
						<td>{{$us->email}}</td>
						<td><a href="/edit/transaksi/{{$us->id}}" class="btn btn-warning"><span class="glyphicon glyphicon-refresh"></span> Edit</a></td>
						<td><a href="/hapus/transaksi/{{$us->id}}" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> Hapus</a></td>
						@endforeach
					</tbody>
				</table>
		</div>
	</div>
@endsection