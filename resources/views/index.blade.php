@extends('layouts.utama')

@section('title', 'App Bengkel')

@section('body')
	<div class="container">
		<h1 class="page-header">Dashboard Bengkel</h1>
		<div class="row">
			<div class="col-md-4">
			<div class="panel panel-primary">
				<div class="panel-heading">
					Emtry Data Transaksi
				</div>
				<div class="panel-body">
					<span class="glyphicon glyphicon-usd dash-text pull-right" style="margin-top: 10px; margin-left: 10px;"></span>
					<span class="dash-text pull-right"></span>
				</div>
				<a href="/new/transaksi">
					<div class="panel-footer">
						<span class="glyphicon glyphicon-new-window"></span> Lihat selengkapnya
					</div>
				</a>
			</div>
		</div>

		<div class="col-md-4">
			<div class="panel panel-primary">
				<div class="panel-heading">
					Manajemen Pengguna
				</div>
				<div class="panel-body">
					<span class="glyphicon glyphicon-user dash-text pull-right" style="margin-top: 10px; margin-left: 10px;"></span>
					<span class="dash-text pull-right"></span>
				</div>
				<a href="/users">
					<div class="panel-footer">
						<span class="glyphicon glyphicon-new-window"></span> Lihat selengkapnya
					</div>
				</a>
			</div>
		</div>

		<div class="col-md-4">
			<div class="panel panel-primary">
				<div class="panel-heading">
					Transaksi Harian
				</div>
				<div class="panel-body">
					<span class="glyphicon glyphicon-usd dash-text pull-right" style="margin-top: 10px; margin-left: 10px;"></span>
					<span class="dash-text pull-right"></span>
				</div>
				<a href="/transaksi">
					<div class="panel-footer">
						<span class="glyphicon glyphicon-new-window"></span> Lihat selengkapnya
					</div>
				</a>
			</div>
		</div>
		</div>
	</div>
@endsection