<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transact extends Model
{
    protected $fillable = [
        'keterangan', 'debit', 'kredit',
    ];
}
