<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transact;

class TransaksiController extends Controller
{
    function index(){
    	$transact = Transact::all();

    	return view('transaksi.index', ['transact' => $transact]);
    }


    function create(Request $req){
    	$transact = new Transact();

    	$transact->keterangan = $req->keterangan;
    	$transact->debit = $req->debit;
    	$transact->kredit = $req->kredit;

    	$transact->save();
    }
}
